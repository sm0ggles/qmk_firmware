#include QMK_KEYBOARD_H

// Define Layers
#define _BASE 0
#define _FNX  1
#define _RGB  2

// Alias' for readability
#define _______ KC_TRNS
#define ___X___ KC_NO


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    // Base Layer
    [_BASE] = LAYOUT(
         KC_ESC,   KC_1,   KC_2,   KC_3,   KC_4,   KC_5,   KC_6,   KC_7,   KC_8,   KC_9,   KC_0,KC_MINS, KC_EQL,KC_BSLS, KC_GRV, KC_INS,
         KC_TAB,   KC_Q,   KC_W,   KC_E,   KC_R,   KC_T,   KC_Y,   KC_U,   KC_I,   KC_O,   KC_P,KC_LBRC,KC_RBRC,        KC_BSPC, KC_DEL,
        KC_LCTL,   KC_A,   KC_S,   KC_D,   KC_F,   KC_G,   KC_H,   KC_J,   KC_K,   KC_L,KC_SCLN,KC_QUOT,  ___X___,         KC_ENT,KC_PGUP,
        KC_LSFT,  ___X___,   KC_Z,   KC_X,   KC_C,   KC_V,   KC_B,   KC_N,   KC_M,KC_COMM, KC_DOT,KC_SLSH,        KC_RSFT,  KC_UP,KC_PGDN,
          KC_CAPS,KC_LGUI,KC_LALT,         KC_SPC, KC_SPC, KC_SPC,                        MO(1),  ___X___,KC_RALT,KC_LEFT,KC_DOWN,KC_RGHT
    ),
    // Fn Layer
    [_FNX] = LAYOUT(
        _______,  KC_F1,  KC_F2,  KC_F3,  KC_F4,  KC_F5,  KC_F6,  KC_F7,  KC_F8,  KC_F9, KC_F10, KC_F11, KC_F12,KC_SLEP,KC_WAKE,  TG(2),
        RGB_MOD,_______,_______,_______,_______,_______,_______,_______,_______,_______,_______,_______,_______,        _______,_______,
        _______,_______,_______,_______,_______,_______,_______,_______,_______,_______,_______,_______,_______,        _______,_______,
        _______,_______,_______,_______,_______,_______,_______,_______,_______,_______,_______,_______,          _______,KC_VOLU,KC_MPLY,
        _______,_______,_______,        _______,_______,_______,                        _______,_______,_______,KC_MRWD,KC_VOLD,KC_MFFD
    ),
    // RGB and BL Layer
    [_RGB] = LAYOUT(
          TG(0),  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  _______,
          ___X___,RGB_HUD,RGB_HUI,___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,          ___X___,BL_TOGG,
          ___X___,RGB_SAD,RGB_SAI,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,          ___X___,RGB_TOG,
          ___X___,  ___X___,RGB_VAD,RGB_VAI,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,  ___X___,RGB_MOD,RGB_RMOD,         ___X___,RGB_M_P,RGB_M_R,
          ___X___,  ___X___,  ___X___,          ___X___,  ___X___,  ___X___,                          ___X___,  ___X___,  ___X___,  ___X___,RGB_M_G,RGB_M_SW
    ),
};
