```
┏━╸┏━╸╻  ┏━┓┏┓╻╻┏━┓   ╻ ╻╺┳┓┏━┓┏━┓
┃  ┣╸ ┃  ┣━┫┃┗┫ ┗━┓   ┏╋┛ ┃┃┣━┓┃┃┃
┗━╸╹  ┗━╸╹ ╹╹ ╹ ┗━┛   ╹ ╹╺┻┛┗━┛┗━┛
```

A Basic XD60 layout with arrow keys and stuff.

![layout](xd60.PNG)

